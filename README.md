# cpp
Boostrap project for C++ containing build system, CI/CD, testing.

## Dev environment
Project is using [Visual Studio Code Remote – Containers](https://code.visualstudio.com/docs/remote/containers).

## Build
```
mkdir -p build && cd build
```
```
cmake ..
make
```

# Unit Tests
```
make
ctest
```