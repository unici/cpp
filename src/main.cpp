#include <boost/program_options.hpp>
#include <iostream>

#include "example_lib.hpp"

namespace po = boost::program_options;

int main(int ac, char *av[]) {
  // Declare the supported options.
  po::options_description desc("Allowed options");
  desc.add_options()("help", "produce help message")("hello-world",
                                                     "display \"Hello World\"");

  po::variables_map vm;
  po::store(po::parse_command_line(ac, av, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  } else if (vm.count("hello-world")) {
    std::cout << "Hello World " << foo() << "\n";
    return 0;
  } else {
    // std::cout << desc << "\n";
    return 1;
  }

  return 0;
}
