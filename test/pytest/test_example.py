import pytest
import subprocess


def test_hello_world():
    result = subprocess.run(['build/src/CppAwesomeProject',  '--hello-world'], capture_output=True)
    assert result.returncode == 0
    assert result.stdout.decode('utf-8') == "Hello World 42\n"

def test_no_parameter_cause_error():
    result = subprocess.run(['build/src/CppAwesomeProject'])
    assert result.returncode != 0



